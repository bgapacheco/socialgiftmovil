package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.adapters.adapterObject;
import com.example.finalmovil.adapters.adapterObjectAddWishList;
import com.example.finalmovil.modelo.GiftModel;
import com.example.finalmovil.modelo.ObjectsModel;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddObjectToWishListActivity extends AppCompatActivity {
    ImageView imgVBack;
    RecyclerView rVObj;
    TextView txtVTitle,txtVAddOtherObject;
    adapterObjectAddWishList adapterRecycler;
    int idWishlist;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_object_to_wish_list);
        imgVBack =  findViewById(R.id.imgVAddWishListObjectBack);
        txtVTitle =  findViewById(R.id.txtVAddWishListObjectName);
        rVObj = findViewById(R.id.rVAddWishListObject);
        txtVAddOtherObject = findViewById(R.id.txtVEndOfListObjectAddToWishList);

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        idWishlist = getIntent().getIntExtra("wishListId",0);

        txtVAddOtherObject.setOnClickListener(v->goToAddNewObject(idWishlist));
        imgVBack.setOnClickListener(v-> finish());

        getWishList();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }

    private void goToAddNewObject(int idWishlist) {
        Intent intent  = new Intent(AddObjectToWishListActivity.this, CreateObject.class);
        intent.putExtra("wishListId", idWishlist);
        startActivity(intent);
    }

    private void getWishList(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/mercadoexpress/api/v1/products";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    ObjectsModel[] objects = gson.fromJson(response, ObjectsModel[].class);
                    llenarRecycler(objects);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) ;
        queue.add(stringRequest);
    }

    private void llenarRecycler(ObjectsModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rVObj.setLayoutManager(layoutManager);
        adapterRecycler = new adapterObjectAddWishList(myDataSet);
        adapterRecycler.setOnItemClickListener(new adapterObjectAddWishList.OnItemClickListener() {
            @Override
            public void onClick(ObjectsModel objectsModel) {
                insertObjectInWishList(objectsModel);
            }



        });
        rVObj.setAdapter(adapterRecycler);

    }

    private void insertObjectInWishList(ObjectsModel Obj){
        JSONObject jsonObject =  createJsonObjectForInsert(Obj.getId());
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/gifts";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                response -> {
                    Toast.makeText(AddObjectToWishListActivity.this, R.string.createSuccessObject, Toast.LENGTH_SHORT).show();
                    finish();
                },
                error -> Toast.makeText(AddObjectToWishListActivity.this, R.string.createFailiureObject, Toast.LENGTH_SHORT).show()){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);
    }

    private  JSONObject createJsonObjectForInsert(int id){
        JSONObject object = new JSONObject();
        try {
            object.put("wishlist_id", idWishlist);
            object.put("product_url", "https://balandrau.salle.url.edu/i3/mercadoexpress/api/v1/products/"+id);
            object.put("priority",33);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        return  object;
    }

}