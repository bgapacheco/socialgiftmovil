package com.example.finalmovil;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.modelo.UserModel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class FragmentProfile extends Fragment {
    String token;
    int id;
    ImageView imgVProfile;
    TextView txtVName,txtMail;
    CardView cardWishLists, cardLogOut,cardFriends,cardGift,cardProfileDelete;
    public FragmentProfile() {
        // Required empty public constructor
    }

    public static FragmentProfile newInstance(String param1, String param2) {
        FragmentProfile fragment = new FragmentProfile();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("preferencias", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        Log.d("toke", token);
        JWT jwt = new JWT(token);
        id = jwt.getClaim("id").asInt();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        getData();

        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgVProfile = view.findViewById(R.id.imgVPerfilProfileImage);
        txtVName = view.findViewById(R.id.txtVPerfilName);
        txtMail =  view.findViewById(R.id.txtVPerfilMail);
        cardWishLists =  view.findViewById(R.id.cardProfileWishList);
        cardFriends =  view.findViewById(R.id.cardFriends);
        cardLogOut = view.findViewById(R.id.cardProfileLogOut);
        cardGift =  view.findViewById(R.id.cardProfileGetItemsReserved);
        cardProfileDelete = view.findViewById(R.id.cardProfileDelete);
        LinearLayout linearEditProfileFragment  = view.findViewById(R.id.linearEditProfileFragment);

        linearEditProfileFragment.setOnClickListener(v->{
            Intent intent = new Intent(FragmentProfile.this.getActivity(), EditProfileActivity.class);
            startActivity(intent);
        });
        cardGift.setOnClickListener(v-> {
            Intent intent = new Intent(FragmentProfile.this.getActivity(), BookedGiftsActivity.class);
            startActivity(intent);
        });

        cardWishLists.setOnClickListener(v-> {
            Intent intent = new Intent(FragmentProfile.this.getActivity(), AllWishlListActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        });

        cardFriends.setOnClickListener(v->{
            Intent intent = new Intent(FragmentProfile.this.getActivity(), FriendActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        });

        cardLogOut.setOnClickListener(v-> {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("token");
            editor.apply();
            Intent intent = new Intent(FragmentProfile.this.getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        });

        cardProfileDelete.setOnClickListener(v-> deleteProfile());
    }

    private void deleteProfile() {
        RequestQueue queue = Volley.newRequestQueue(FragmentProfile.this.getContext());
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users";
        StringRequest request = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    getActivity().finish();
                    Intent intent = new Intent(FragmentProfile.this.getActivity(), LoginActivity.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), R.string.profileDeleted, Toast.LENGTH_SHORT).show();
                },
                error -> {
                    Log.e("Error", error.getMessage());
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        queue.add(request);

    }

    public void getData(){
        RequestQueue queue = Volley.newRequestQueue(FragmentProfile.this.getContext());
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/"+id;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    UserModel user = gson.fromJson(response, UserModel.class);
                    setUpData(user);
                    Log.d("id", user.getName());
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };

        queue.add(stringRequest);
    }

    public void setUpData(UserModel user){
        txtVName.setText(user.getName() +" "+ user.getLastName());
        txtMail.setText(user.getEmail());
        Picasso.get()
                .load(user.getImage())
                .error(R.drawable.ic_default_user)
                .into(imgVProfile);
    }
    @Override
    public void onResume() {
        super.onResume();
        getData();
    }
}