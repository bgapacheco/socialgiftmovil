package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.R;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateWishListActivity extends AppCompatActivity {
    int id ;
    String token;
    EditText edtName, edtDescription;
    JSONObject wishListForEdit;
    Button btnAction;
    ImageView imgVBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_wish_list);
        btnAction = findViewById(R.id.btnCreateWishList);
        edtName = findViewById(R.id.edtTNameCreateWishList);
        edtDescription = findViewById(R.id.edtTDescriptionCreateWishList);
        imgVBack = findViewById(R.id.imgVWishListActivityBack);

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");

        id =  getIntent().getIntExtra("wishlistId",0);
        if (id != 0){
            getWishList();
            btnAction.setText(R.string.EditWishList);
        }
        btnAction.setOnClickListener(view -> {
            try {
                ActionWishlist();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });
        imgVBack.setOnClickListener(v -> {
            finish();
        });
    }

    private void getWishList(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/wishlists/"+id;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    WishListModel wishlist = gson.fromJson(response, WishListModel.class);
                    edtName.setText(wishlist.getName());
                    edtDescription.setText(wishlist.getDescription());
                    createJsonObjectForEdit(wishlist);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization","Bearer "+  token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }
    private JSONObject createJsonObjectForCreate()  {
        JSONObject wishList = new JSONObject();
        String strName =  edtName.getText().toString().trim();
        String strDesc =  edtDescription.getText().toString().trim();
        try {
            wishList.put("name", strName);
            wishList.put("description", strDesc);
        }
         catch (JSONException e) {
             throw new RuntimeException(e);
         }
        return  wishList;
    }
    private void createJsonObjectForEdit(WishListModel wishListModel)  {
        wishListForEdit = new JSONObject();
        String strName =  edtName.getText().toString().trim();
        String strDesc =  edtDescription.getText().toString().trim();

        try {

            wishListForEdit.put("name", strName);
            wishListForEdit.put("description", strDesc);
            wishListForEdit.put("gifts", wishListModel.getGifts());
        }
        catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
    private void ActionWishlist() throws JSONException {

        if (id == 0){
            CreateWishList();
        }else {
            getWishList();
            EditWishList();
        }

    }
    private void CreateWishList(){

        JSONObject jsonWishListCreate = createJsonObjectForCreate();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/wishlists";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonWishListCreate,
                response -> {
                    Toast.makeText(CreateWishListActivity.this, R.string.createSuccesWishList, Toast.LENGTH_SHORT).show();
                    finish();
                },
                error -> Toast.makeText(CreateWishListActivity.this, R.string.createFailiureWishList, Toast.LENGTH_SHORT).show()){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
    private void EditWishList() throws JSONException {
        wishListForEdit.remove("name");
        wishListForEdit.put("name", edtName.getText().toString().trim());
        wishListForEdit.remove("description");
        wishListForEdit.put("description", edtDescription.getText().toString().trim());
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/wishlists/"+id;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, wishListForEdit,
                response -> {
                    Log.d("hola", wishListForEdit.toString());
                    Toast.makeText(CreateWishListActivity.this, R.string.editSuccesWishList, Toast.LENGTH_SHORT).show();
                    finish();
                },
                error -> {
                    Toast.makeText(CreateWishListActivity.this, R.string.editFailiureWishList, Toast.LENGTH_SHORT).show();
                    finish();
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
}