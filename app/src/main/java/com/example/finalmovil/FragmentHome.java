package com.example.finalmovil;

import static android.content.Context.MODE_PRIVATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterNewWishListHome;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class FragmentHome extends Fragment {

    int id = 0;
    String token = "";
    WishListModel[] wishlist;
    RecyclerView rvHome;
    private adapterNewWishListHome adapterRecycler;
    public FragmentHome() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("preferencias", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        Log.d("toke", token);
        JWT jwt = new JWT(token);
        id = jwt.getClaim("id").asInt();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        rvHome = rootView.findViewById(R.id.rvHome);
        return rootView;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getWishList();
        super.onViewCreated(view, savedInstanceState);
    }
    private void llenarRecycler(WishListModel[] myDataSet) {
        //        Log.d("hola", "entro");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        rvHome.setLayoutManager(layoutManager);
        adapterRecycler = new adapterNewWishListHome(myDataSet);
        adapterRecycler.setOnItemClickListener(
                position -> {
                    showWishlist(position);
                    Log.d("IdWish", String.valueOf(position));
                });
        rvHome.setAdapter(adapterRecycler);
    }

    private void getWishList(){
        RequestQueue queue = Volley.newRequestQueue(FragmentHome.this.getContext());
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/wishlists";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    wishlist = gson.fromJson(response, WishListModel[].class);
                    llenarRecycler(wishlist);
                    Log.d("wishlistsNum", "Response is: " + wishlist.length);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };

        queue.add(stringRequest);
    }
    private void showWishlist(int id){
        Log.d("wish", String.valueOf(wishlist.length));
        Intent intent = new Intent(FragmentHome.this.getActivity(), WishListActivity.class);
        intent.putExtra("wishlistId", id);
        startActivity(intent);
    }
}
