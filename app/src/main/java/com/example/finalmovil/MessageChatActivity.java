package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterMessageChat;
import com.example.finalmovil.modelo.MessageModel;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MessageChatActivity extends AppCompatActivity {
    ImageView imgVBack;
    TextView txtVUserName;
    RecyclerView rvMessage;
    adapterMessageChat adapterRecycler;
    ImageView imgVSend;
    EditText edtMessage;
    String token;
    int idUserOwner;
    int idOtherUser;
    String userNameOtherUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_chat);

        imgVBack = findViewById(R.id.imgVMessageChatBack);
        txtVUserName = findViewById(R.id.txtVMessageChatName);
        rvMessage =findViewById(R.id.rVMessageChat);
        edtMessage = findViewById(R.id.edtMessage);
        imgVSend = findViewById(R.id.imgVSendMessage);

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        JWT jwt = new JWT(token);
        idUserOwner = jwt.getClaim("id").asInt();
        idOtherUser = getIntent().getIntExtra("idUser",0);
        userNameOtherUser = getIntent().getStringExtra("userName");
        txtVUserName.setText(userNameOtherUser);
        getAllMessages(idOtherUser);
        imgVSend.setOnClickListener(v-> postMessage());
        imgVBack.setOnClickListener(v-> finish());
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // Lógica del método a ejecutar
                getAllMessages(idOtherUser);
            }
        };
        // Programar la tarea para que se ejecute cada 2 segundos (2000 milisegundos)
        timer.scheduleAtFixedRate(task, 0, 1000);
    }

    private void getAllMessages(int id){
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/messages/" + id;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        MessageModel[] messages = gson.fromJson(response, MessageModel[].class);
                        fillRecycler(messages);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejar el error de la solicitud
                        Log.e("Error", error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token); // Agregar el encabezado con el token
                return headers;
            }
        };

        requestQueue.add(stringRequest);
    }
    private void postMessage(){
        JSONObject jsonWishListCreate = createJsonObjectForCreate();
        edtMessage.setText("");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/messages";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonWishListCreate,
                response -> {
                },
                error -> {Toast.makeText(MessageChatActivity.this, R.string.createFailiureObject, Toast.LENGTH_SHORT).show();
//        Log.e("Error", error.getMessage());
        Log.d("UserId", String.valueOf(idOtherUser));}){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
    private  JSONObject createJsonObjectForCreate(){
        JSONObject wishList = new JSONObject();
        String strContent =  edtMessage.getText().toString().trim();
        try {
            wishList.put("content", strContent);
            wishList.put("user_id_send", idUserOwner);
            wishList.put("user_id_recived", idOtherUser);
        }
        catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return  wishList;

    }



    private void fillRecycler(MessageModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(MessageChatActivity.this);
        layoutManager.setStackFromEnd(true);
        rvMessage.setLayoutManager(layoutManager);
        adapterRecycler = new adapterMessageChat(myDataSet,idUserOwner);
        rvMessage.setAdapter(adapterRecycler);
    }
}