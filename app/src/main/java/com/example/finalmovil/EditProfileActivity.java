package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.modelo.UserModel;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {
    EditText edtName, edtLastName;
    Button btnEdit;
    ImageView imgVBack;
    String token;
    JSONObject profileForEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        JWT jwt = new JWT(token);
        int idUser = jwt.getClaim("id").asInt();
        imgVBack = findViewById(R.id.imgVEditProfileBack);
        edtName = findViewById(R.id.edtTNameEditProfile);
        edtLastName = findViewById(R.id.edtLastNameEditProfile);
        btnEdit = findViewById(R.id.btnEditProfile);
        getPerfil(idUser);
        imgVBack.setOnClickListener(v-> finish());

        btnEdit.setOnClickListener(v-> {
            try {
                editarPefil();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });
    }


    private void getPerfil(int id){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/"+id;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    UserModel user = gson.fromJson(response, UserModel.class);
                    edtName.setText(user.getName());
                    edtLastName.setText(user.getLastName());
                    createJsonToEdit(user);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization","Bearer "+  token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }
    private void createJsonToEdit(UserModel user){
        profileForEdit = new JSONObject();
        try {

            profileForEdit.put("email", user.getEmail());
            profileForEdit.put("image", user.getImage());
        }
        catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
    private void editarPefil() throws JSONException {
        if (!edtName.getText().toString().matches("") && !edtLastName.getText().toString().matches("")){

            profileForEdit.put("name", edtName.getText().toString());
            profileForEdit.put("last_name", edtLastName.getText().toString());
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, profileForEdit,
                    response -> {
                        Toast.makeText(EditProfileActivity.this, R.string.profileEditSuccess, Toast.LENGTH_SHORT).show();
                        finish();
                    },
                    error -> {
                        Toast.makeText(EditProfileActivity.this, R.string.profileEditFailure, Toast.LENGTH_SHORT).show();
                        finish();
                    }){
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " +token);
                    return headers;
                }
            };

            requestQueue.add(request);
        }else {
            Toast.makeText(this, R.string.fillAllThedata, Toast.LENGTH_SHORT).show();
        }

    }

}