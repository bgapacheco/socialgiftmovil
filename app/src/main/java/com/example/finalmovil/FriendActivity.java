package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterFriend;
import com.example.finalmovil.modelo.UserModel;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class FriendActivity extends AppCompatActivity {
    RecyclerView rVFriends;
    private adapterFriend adapterRecycler;
    ImageView imgVBack;
    TextView txtVFriends;
    int id=1;
    boolean isOwner = true;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);
        rVFriends = findViewById(R.id.rVAllFriends);
        imgVBack =  findViewById(R.id.imgVAllFriendsBack);
        txtVFriends = findViewById(R.id.friendActivityRequests);
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        JWT jwt = new JWT(token);
        id = jwt.getClaim("id").asInt();
        if(id != getIntent().getIntExtra("id", 0)){
            isOwner= false;
            id = getIntent().getIntExtra("id", 0);
            txtVFriends.setVisibility(View.GONE);
        }else {
            txtVFriends.setVisibility(View.VISIBLE);
            txtVFriends.setOnClickListener(v-> sendToFriendRequestActivity());
        }
        imgVBack.setOnClickListener(view -> finish());
        getFriends();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFriends();
    }

    private void sendToFriendRequestActivity() {
        Intent intent =  new Intent(FriendActivity.this, FriendRequestActivity.class);
        startActivity(intent);
    }

    private void getFriends(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/"+id+"/friends";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    UserModel[] friends = gson.fromJson(response, UserModel[].class);
                    fillRecycler(friends);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization","Bearer "+  token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }
    private void fillRecycler(UserModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rVFriends.setLayoutManager(layoutManager);
        adapterRecycler = new adapterFriend(myDataSet,isOwner);
        adapterRecycler.setOnItemClickListener(new adapterFriend.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                showFriend(position);
                Log.d("IdWishItem", String.valueOf(position));
            }
            @Override
            public void onRemove(int position) {
                Log.d("IdWishRemove", String.valueOf(position));
                deleteFriend(position);

            }
        });
        rVFriends.setAdapter(adapterRecycler);
    }
    private void showFriend(int id){
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("userId", id);
        startActivity(intent);
    }

    private void deleteFriend(int id){
        RequestQueue requestQueue = Volley.newRequestQueue(FriendActivity.this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/friends/"+id;
        StringRequest request = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    getFriends();
                    Toast.makeText(FriendActivity.this, R.string.friendDeleted, Toast.LENGTH_SHORT).show();
                },
                error -> {
                    Toast.makeText(FriendActivity.this, R.string.GenericError, Toast.LENGTH_SHORT).show();
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);

    }
}