package com.example.finalmovil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterNewWishListHome;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    String strUserName = "";
    String strPassword = "";
    TextView txt, txtVRegisterLogin;

    Boolean bolRememberLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txt = findViewById(R.id.txtVLoginEmail);
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        EditText edtTUsername = findViewById(R.id.edtLoginUsername);
        EditText edtTPassword = findViewById(R.id.edtLoginPassword);
        CheckBox chkBRemberLogin = findViewById(R.id.chkBLoginRememberUser);
        txtVRegisterLogin =  findViewById(R.id.txtVRegisterLogin);
        Button btnLogin = findViewById(R.id.btnLoginSignIn);
        String usuario = sharedPreferences.getString("email", getIntent().getStringExtra("email"));
        edtTUsername.setText(usuario);

        txtVRegisterLogin.setOnClickListener(view -> sendToRegister());
        btnLogin.setOnClickListener(v ->{
            strUserName = edtTUsername.getText().toString();
            strPassword =  edtTPassword.getText().toString();
            bolRememberLogin = chkBRemberLogin.isChecked();
            login(strUserName, strPassword);
        });


    }
    private void login(String usuario, String contraseña) {

        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/login";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", usuario);
            jsonObject.put("password", contraseña);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                response -> {
                    try {
                        if (bolRememberLogin == true){
                            SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email", usuario);
                            editor.apply();
                        } else {
                            SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email", "");
                            editor.apply();
                        }
                        nextActivity(response);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                },
                error -> {
//                    if(error.networkResponse.statusCode == 401){
//                        Toast.makeText(LoginActivity.this,R.string.error401,Toast.LENGTH_SHORT).show();
//                    }else{
//                        Toast.makeText(LoginActivity.this,R.string.error,Toast.LENGTH_SHORT).show();
//
//                    }
                });

        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }
    private void nextActivity(JSONObject response) throws JSONException {
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", response.getString("accessToken"));
        editor.apply();

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void sendToRegister(){
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
        finish();
    }

}