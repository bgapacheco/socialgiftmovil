package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.adapters.adapterFriend;
import com.example.finalmovil.modelo.UserModel;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class SearchUserActivity extends AppCompatActivity {
    ImageView imgVBack;
    RecyclerView rvUsers;
    String userData;
    adapterFriend adapterRecycler;
    TextView txtVTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        imgVBack = findViewById(R.id.imgVSearchUserActivityBack);
        rvUsers = findViewById(R.id.rVSearchUserActivity);
        txtVTop = findViewById(R.id.txtVSearchUserActivity);
        userData = getIntent().getStringExtra("user");
        txtVTop.setText(getString(R.string.searchingUser) + " " +userData);
        imgVBack.setOnClickListener(v-> finish());
        searchUser(userData);

    }
    private void fillRecycler(UserModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvUsers.setLayoutManager(layoutManager);
        adapterRecycler = new adapterFriend(myDataSet,false);
        adapterRecycler.setOnItemClickListener(new adapterFriend.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                sendToProfile(position);
            }
            @Override
            public void onRemove(int position) {

            }
        });
        rvUsers.setAdapter(adapterRecycler);
    }
    public void searchUser(String user){
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "");

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/search?s="+user;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    UserModel[] userModel = gson.fromJson(response, UserModel[].class);
                    fillRecycler(userModel);
                    Log.d("userSearchedID", "Response is: " + userModel[0].getName());
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };

        requestQueue.add(stringRequest);

    }
    private void  sendToProfile(int idUser){
        Intent intent =  new Intent(this, ProfileActivity.class);
        intent.putExtra("userId", idUser);
        startActivity(intent);
    }
}