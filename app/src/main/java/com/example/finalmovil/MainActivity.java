package com.example.finalmovil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.modelo.UserModel;
import com.example.finalmovil.modelo.WishListModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView bottomNavigationView;
    ConstraintLayout layoutCentralHome;
    ImageView imgBtnSearch;
    EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layoutCentralHome = findViewById(R.id.layoutCentralHome);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        imgBtnSearch = findViewById(R.id.ImgVBtnSearch);

        edtSearch = findViewById(R.id.edtHomeSearchTopNavBar);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.homeNav);

        disableEditTextToSearch();
        imgBtnSearch.setOnClickListener(view -> {
            if(edtSearch.getAlpha() == 1){
                sendToProfileSearch(edtSearch.getText().toString());
            }
            enableEditTextToSearch();
        });
        layoutCentralHome.setOnClickListener(view -> {
            edtSearch.setAlpha(0);
        });
    }
    public void enableEditTextToSearch(){
        edtSearch.setEnabled(true);
        edtSearch.setAlpha(1);
        edtSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
    }
    public void disableEditTextToSearch(){
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        edtSearch.setEnabled(false);
        edtSearch.setAlpha(0);
    }
    FragmentHome HomeFragment = new FragmentHome();
    FragmentProfile ProfileFragment = new FragmentProfile();
    FragmentChats ChatsFragment = new FragmentChats();

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.homeNav:
                getSupportFragmentManager().beginTransaction().replace(R.id.flFragment, HomeFragment).commit();
                return true;
            case R.id.profileNav:
                getSupportFragmentManager().beginTransaction().replace(R.id.flFragment, ProfileFragment).commit();
                return true;
            case R.id.chatNav:
                getSupportFragmentManager().beginTransaction().replace(R.id.flFragment, ChatsFragment).commit();
                return true;

        }
        return false;
    }

    private void  sendToProfileSearch(String user){
        Intent intent =  new Intent(this, SearchUserActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
    }

}