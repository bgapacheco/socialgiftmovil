package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterObject;
import com.example.finalmovil.modelo.GiftModel;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WishListActivity extends AppCompatActivity {

    TextView txtVTitle;
    ImageView imgVWishListBack;
    RecyclerView rvObject;
    ImageView imgVAdd;
    private adapterObject adapterRecycler;
    boolean isOwner;
    int iduser;
    int idWishlist;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        rvObject =  findViewById(R.id.rVWishListObject);
        imgVWishListBack = findViewById(R.id.imgVWishListBack);
        txtVTitle =  findViewById(R.id.txtVWishWishListName);
        imgVAdd = findViewById(R.id.imgVWishListAddNewObj);
        idWishlist =  getIntent().getIntExtra("wishlistId", 1);
        isOwner = getIntent().getBooleanExtra("isOwner",false);

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        JWT jwt = new JWT(token);
        iduser = jwt.getClaim("id").asInt();
        if(!isOwner){
//            isOwner= false;
            imgVAdd.setVisibility(View.GONE);
        }else {
            imgVAdd.setVisibility(View.VISIBLE);

            imgVAdd.setOnClickListener(view -> showAddObjectActivity());
        }

        imgVWishListBack.setOnClickListener(v-> {
            finish();
        });

        getWishList();

        imgVWishListBack.setOnClickListener(v -> {
            finish();
        });
    }
    public void onResume(){
        super.onResume();
        getWishList();
    }

    private void showAddObjectActivity() {
        Intent intent = new Intent(WishListActivity.this, AddObjectToWishListActivity.class);
        intent.putExtra("wishListId", idWishlist);
        startActivity(intent);
    }

    private void getWishList(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/wishlists/"+idWishlist;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    WishListModel wishlist = gson.fromJson(response, WishListModel.class);
                    txtVTitle.setText(wishlist.getName());
                    llenarRecycler(wishlist.getGifts());
                    Log.d("wishlistsNum", "Response is: " + wishlist.getId());
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization","Bearer "+  token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }
    private void llenarRecycler(List<GiftModel> myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvObject.setLayoutManager(layoutManager);
        adapterRecycler = new adapterObject(myDataSet,isOwner);
        adapterRecycler.setOnItemClickListener(new adapterObject.OnItemClickListener() {
            @Override
            public void onRemove(int position) {
                Log.d("IdWishRemove", String.valueOf(position));
                deleteObject(position);

            }

            @Override
            public void onBooked(int position) {
                reserveObject(position);
                Log.d("Booked", String.valueOf(position));
            }

            @Override
            public void onUndoBooked(int position) {
                undoReserveObject(position);
                Log.d("unBooked", String.valueOf(position));

            }


        });
        rvObject.setAdapter(adapterRecycler);
    }

    private void undoReserveObject(int position) {
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/gifts/"+position+"/book";
        RequestQueue requestQueue = Volley.newRequestQueue(WishListActivity.this);

        StringRequest request = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    getWishList();
                    Toast.makeText(WishListActivity.this, R.string.unBooked, Toast.LENGTH_SHORT).show();
                },
                error -> {

                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);

    }

    private void reserveObject(int position) {
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/gifts/"+position+"/book";
        RequestQueue requestQueue = Volley.newRequestQueue(WishListActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                response -> {
                    getWishList();
                    Toast.makeText(WishListActivity.this, R.string.booked, Toast.LENGTH_SHORT).show();
                },
                error -> {

                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);

    }

    private void deleteObject(int id){
        RequestQueue requestQueue = Volley.newRequestQueue(WishListActivity.this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/gifts/"+id;
        StringRequest request = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    getWishList();
                    Toast.makeText(WishListActivity.this, R.string.objectDeleted, Toast.LENGTH_SHORT).show();
                },
                error -> {
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);
    }
}