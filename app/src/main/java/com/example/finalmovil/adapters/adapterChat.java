package com.example.finalmovil.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalmovil.R;
import com.example.finalmovil.modelo.UserModel;
import com.squareup.picasso.Picasso;

public class adapterChat extends RecyclerView.Adapter<adapterChat.ViewHolder>{
    UserModel[] user;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVProfile;
        TextView txtVName;
        public ViewHolder(View view) {
            super(view);
            imgVProfile =  view.findViewById(R.id.imgVChatProfileImage);
            txtVName =  view.findViewById(R.id.txtVChatName);
        }

    }
    public adapterChat(UserModel[] userModels){
        this.user = userModels;
    }
    @NonNull
    @Override
    public adapterChat.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_chat, parent, false);
        return new adapterChat.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterChat.ViewHolder holder, int position) {

        holder.txtVName.setText(user[position].getName() + " " +user[position].getLastName());
        Picasso.get()
                .load(user[position].getImage())
                .into(holder.imgVProfile);
        holder.itemView.setOnClickListener(v->{
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(user[position].getId(), (user[position].getName() + " " +user[position].getLastName()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return user.length;
    }

    private adapterChat.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position,String userName);
    }
    public void setOnItemClickListener(adapterChat.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}
