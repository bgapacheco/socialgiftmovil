package com.example.finalmovil.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.R;
import com.example.finalmovil.modelo.ObjectsModel;
import com.example.finalmovil.modelo.UserModel;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class adapterNewWishListHome extends RecyclerView.Adapter<adapterNewWishListHome.ViewHolder>{

    WishListModel [] wishlists;
    private OnItemClickListener onItemClickListener;
    String token;
    String strDescription;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVProfile;
        ImageView imgVWishList;
        TextView txtVDescription;
        CardView cardWishListHome;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            cardWishListHome = (CardView) view.findViewById(R.id.cardWishListHome);
            imgVProfile = (ImageView) view.findViewById(R.id.imgVWishlisthomeProfileImage);
            imgVWishList =  (ImageView) view.findViewById(R.id.imgVWishlisthomeImage);
            txtVDescription = (TextView) view.findViewById(R.id.txtVWishlistHomeText);
        }

    }
    public adapterNewWishListHome(WishListModel[] wishListModels){
        Collections.reverse(Arrays.asList(wishListModels));
        wishlists = wishListModels;
    }

    @NonNull
    @Override
    public adapterNewWishListHome.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_newwishlisthome, parent, false);
        SharedPreferences sharedPreferences = parent.getContext().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterNewWishListHome.ViewHolder holder, int position) {
        getUser(holder, wishlists[position]);
        holder.itemView.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(wishlists[position].getId());
            }
        });
    }



    @Override
    public int getItemCount() {
        return wishlists.length;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    private void getUser(adapterNewWishListHome.ViewHolder holder, WishListModel wishListModel) {
        RequestQueue requestQueue = Volley.newRequestQueue(holder.itemView.getContext());
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/"+ wishListModel.getUserId();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null,
                response -> {
                    Gson gson = new Gson();
                    UserModel user = gson.fromJson(response.toString(), UserModel.class);
                    String sourceString = "<b>" + user.getName() + "</b> agregó una nueva wishList <b>"+wishListModel.getName()+"</b> con <b>" +wishListModel.getGifts().size()+"</b> regalos" ;
                    holder.txtVDescription.setText(HtmlCompat.fromHtml(sourceString, HtmlCompat.FROM_HTML_MODE_LEGACY));
                    if(!user.getImage().isEmpty()){
                        Picasso.get()
                                .load(user.getImage())
                                .error(R.drawable.ic_default_user)
                                .into(holder.imgVProfile);
                    }else{
                        holder.imgVProfile.setImageResource(R.drawable.ic_default_user);
                    }

                },
                error -> {

                }

        ){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization","Bearer "+  token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
}
