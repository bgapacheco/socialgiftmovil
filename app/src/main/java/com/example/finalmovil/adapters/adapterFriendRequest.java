package com.example.finalmovil.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalmovil.R;
import com.example.finalmovil.modelo.UserModel;
import com.squareup.picasso.Picasso;

public class adapterFriendRequest extends RecyclerView.Adapter<adapterFriendRequest.ViewHolder>{
    UserModel[] friendRequests;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVProfilePicture, imgVAcceptRequest, imgVDeclineRequest;
        TextView txtVName;
        public ViewHolder(View view) {
            super(view);
            imgVProfilePicture = view.findViewById(R.id.imgVFriendRequestImage);
            imgVAcceptRequest = view.findViewById(R.id.imgVFriendRequestAccept);
            imgVDeclineRequest = view.findViewById(R.id.imgVFriendRequestRemove);
            txtVName = view.findViewById(R.id.txtVFriendRequestName);

        }

    }
    public adapterFriendRequest(UserModel[] friendRequest){
        this.friendRequests = friendRequest;

    }
    @NonNull
    @Override
    public adapterFriendRequest.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_friend_request, parent, false);
        return new adapterFriendRequest.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterFriendRequest.ViewHolder holder, int position) {
        holder.txtVName.setText(friendRequests[0].getName());
        if (friendRequests[position].getImage() !=null){
            Picasso.get().load(friendRequests[position].getImage()).into(holder.imgVProfilePicture);
        }else {
            holder.imgVProfilePicture.setImageResource(R.drawable.ic_default_user);
        }
        holder.imgVAcceptRequest.setOnClickListener(v-> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickAccept(friendRequests[position].getId());
            }
        });
        holder.imgVDeclineRequest.setOnClickListener(v-> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickRemove(friendRequests[position].getId());
            }
        });
        holder.itemView.setOnClickListener(v->{

            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(friendRequests[position].getId());
            }

        });
    }


    @Override
    public int getItemCount() {
        return friendRequests.length;
    }

    private adapterFriendRequest.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void  onClickAccept(int position);
        void  onClickRemove(int position);
    }
    public void setOnItemClickListener(adapterFriendRequest.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}
