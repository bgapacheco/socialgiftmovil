package com.example.finalmovil.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalmovil.R;
import com.example.finalmovil.modelo.GiftModel;
import com.example.finalmovil.modelo.MessageModel;

public class adapterMessageChat extends RecyclerView.Adapter<adapterMessageChat.ViewHolder>{

    MessageModel[] messages;
    int userid;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtVItem;
        private Space spacerItemLeft;
        private Space spacerItemRight;

        public ViewHolder(View view) {
            super(view);
            txtVItem = view.findViewById(R.id.txtVItem);
            spacerItemLeft = view.findViewById(R.id.spacerItemLeft);
            spacerItemRight = view.findViewById(R.id.spacerItemRight);
        }

    }
    public adapterMessageChat(MessageModel[] messageModel,int userId){
            this.messages = messageModel;
            this.userid = userId;
    }
    @NonNull
    @Override
    public adapterMessageChat.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_message_chat, parent, false);
        return new adapterMessageChat.ViewHolder(vista);    }

    @Override
    public void onBindViewHolder(@NonNull adapterMessageChat.ViewHolder holder, int position) {
        ViewGroup.LayoutParams layoutParamsLeft = holder.spacerItemLeft.getLayoutParams();
        ViewGroup.LayoutParams layoutParamsRight = holder.spacerItemRight.getLayoutParams();

        holder.txtVItem.setText(messages[position].getContent());
        if (messages[position].getUserIdSend() == userid) {

            layoutParamsLeft.width = 50;
            holder.spacerItemLeft.setLayoutParams(layoutParamsLeft);

            layoutParamsRight.width = 0;
            holder.spacerItemRight.setLayoutParams(layoutParamsRight);

        }else {
            layoutParamsLeft.width = 0;
            holder.spacerItemLeft.setLayoutParams(layoutParamsLeft);

            layoutParamsRight.width = 50;
            holder.spacerItemRight.setLayoutParams(layoutParamsRight);
        }

    }

    @Override
    public int getItemCount() {
        return messages.length;
    }
}
