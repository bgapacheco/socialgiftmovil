package com.example.finalmovil.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalmovil.R;
import com.example.finalmovil.modelo.GiftModel;
import com.example.finalmovil.modelo.UserModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class adapterFriend extends RecyclerView.Adapter<adapterFriend.ViewHolder>{
    UserModel[] user;
    boolean isOwner;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVFriend ;
        ImageView imgVFriendRemove ;
        TextView txtVFriendName;

        public ViewHolder(View view) {
            super(view);
            imgVFriend = view.findViewById(R.id.imgVFriendImage);
            imgVFriendRemove = view.findViewById(R.id.imgVFriendRemove);
            txtVFriendName = view.findViewById(R.id.txtVFriendName);
        }

    }
    public adapterFriend(UserModel[] userModels, boolean isOwner){
        this.user = userModels;
        this.isOwner = isOwner;
    }
    @NonNull
    @Override
    public adapterFriend.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_friend, parent, false);
        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterFriend.ViewHolder holder, int position) {
        holder.txtVFriendName.setText(user[position].getName() + " " +user[position].getLastName());
        String imagePath = user[position].getImage();
        if (imagePath != null && !imagePath.isEmpty()) {
            Picasso.get()
                    .load(imagePath)
                    .error(R.drawable.ic_default_user)
                    .into(holder.imgVFriend);
        } else {
            holder.imgVFriend.setImageResource(R.drawable.ic_default_user);
        }
        holder.imgVFriendRemove.setVisibility(View.GONE);

        if (isOwner){
            holder.imgVFriendRemove.setVisibility(View.VISIBLE);
            holder.imgVFriendRemove.setOnClickListener(v->{
                if (onItemClickListener != null) {
                    onItemClickListener.onRemove(user[position].getId());
                }
            });
        }
        holder.itemView.setOnClickListener(v->{

            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(user[position].getId());
            }

        });
    }

    @Override
    public int getItemCount() {
        return user.length;
    }
    private adapterFriend.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void  onRemove(int position);
    }
    public void setOnItemClickListener(adapterFriend.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}