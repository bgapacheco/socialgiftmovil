package com.example.finalmovil.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalmovil.R;
import com.example.finalmovil.modelo.WishListModel;

public class adapterWishlist  extends RecyclerView.Adapter<adapterWishlist.ViewHolder>{
    WishListModel[] wishListModel ;
    Boolean isOwner;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtVTitle;
        ImageView imgVWishlistRemove, imgVWishlistEdit;
        public ViewHolder(View view) {
            super(view);
            txtVTitle = view.findViewById(R.id.txtVWishlistName);
            imgVWishlistRemove = view.findViewById(R.id.imgVWishlistRemove);
            imgVWishlistEdit = view.findViewById(R.id.imgVWishlistEdit);
        }

    }
    public adapterWishlist(WishListModel[] wishListModel, boolean isOwner){
        this.wishListModel = wishListModel;
        this.isOwner = isOwner;
    }
    @NonNull
    @Override
    public adapterWishlist.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_wishlist, parent, false);
        return new adapterWishlist.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterWishlist.ViewHolder holder, int position) {
        holder.txtVTitle.setText(wishListModel[position].getName());
        if (!isOwner) {
            holder.imgVWishlistEdit.setVisibility(View.GONE);
            holder.imgVWishlistRemove.setVisibility(View.GONE);
        }else{
            holder.imgVWishlistRemove.setVisibility(View.VISIBLE);
            holder.imgVWishlistEdit.setVisibility(View.VISIBLE);

            holder.imgVWishlistEdit.setOnClickListener(view ->{

                if (onItemClickListener != null) {
                    onItemClickListener.onEdit(wishListModel[position].getId());
                }

            } );

            holder.imgVWishlistRemove.setOnClickListener(v -> {
                // Lógica para el click en imgVWishlistRemove
                // Puedes acceder a la posición del elemento haciendo referencia a "position"
                if (onItemClickListener != null) {
                    onItemClickListener.onRemove(wishListModel[position].getId());
                }
            });
        }
        holder.itemView.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(wishListModel[position].getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return wishListModel.length;
    }

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void  onRemove(int position);
        void onEdit(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}
