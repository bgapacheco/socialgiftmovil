package com.example.finalmovil.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalmovil.R;
import com.example.finalmovil.modelo.ObjectsModel;
import com.example.finalmovil.modelo.UserModel;
import com.squareup.picasso.Picasso;

public class adapterObjectAddWishList extends RecyclerView.Adapter<adapterObjectAddWishList.ViewHolder>{
    ObjectsModel[] objectsModels;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVObj ;
        ImageView imgVObjectAdd ;
        TextView txtVObjectName;
        TextView txtVObjectDescription;


        public ViewHolder(View view) {
            super(view);
            imgVObj = view.findViewById(R.id.imgVObjectAddToWishListImage);
            imgVObjectAdd =  view.findViewById(R.id.imgVObjectAddToWishList);
            txtVObjectName = view.findViewById(R.id.txtVObjectAddToWishListName);
            txtVObjectDescription = view.findViewById(R.id.txtVObjectAddToWishListDescription);

        }

    }
    public adapterObjectAddWishList(ObjectsModel[] objectsModels){
        this.objectsModels = objectsModels;
    }
    @NonNull
    @Override
    public adapterObjectAddWishList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_object_add_wishlist, parent, false);
        return new adapterObjectAddWishList.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterObjectAddWishList.ViewHolder holder, int position) {

        if (objectsModels[position].getPhoto() != null){
            Picasso.get().load(objectsModels[position].getPhoto()).error(R.drawable.logo).into(holder.imgVObj);
        } else {
            holder.imgVObj.setImageResource(R.drawable.logo);
        }

        holder.txtVObjectName.setText(objectsModels[position].getName());

        holder.txtVObjectDescription.setText(objectsModels[position].getDescription());
        holder.imgVObjectAdd.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(objectsModels[position]);
            }
        });

    }

    @Override
    public int getItemCount() {
        return objectsModels.length;
    }
    private adapterObjectAddWishList.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onClick(ObjectsModel objectsModel);
    }
    public void setOnItemClickListener(adapterObjectAddWishList.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}
