package com.example.finalmovil.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.R;
import com.example.finalmovil.modelo.GiftModel;
import com.example.finalmovil.modelo.ObjectsModel;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class adapterObject  extends RecyclerView.Adapter<adapterObject.ViewHolder>{

    List<GiftModel>  giftModels;
    boolean isOwner,isForBookedGift;
    String token;
    private adapterObject.OnItemClickListener onItemClickListener;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVObj ;
        ImageView imgVObjectAction;
        TextView txtVObjectName;
        TextView txtVObjectDescription;


        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            imgVObj = view.findViewById(R.id.imgVObjectImage);
            imgVObjectAction =  view.findViewById(R.id.imgVObjectRemove);
            txtVObjectName = view.findViewById(R.id.txtVObjectName);
            txtVObjectDescription = view.findViewById(R.id.txtVObjectDescription);

        }

    }
    public adapterObject(List<GiftModel> giftModels, boolean isOwner){
        this.giftModels = giftModels;
        this.isOwner = isOwner;
    }
    public adapterObject(List<GiftModel> giftModels, boolean isOwner, boolean isForBookedGift,String token){
        this.giftModels = giftModels;
        this.isOwner = isOwner;
        this.isForBookedGift = isForBookedGift;
        this.token = token;
    }
    @NonNull
    @Override
    public adapterObject.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_object, parent, false);
        return new adapterObject.ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull adapterObject.ViewHolder holder, int position) {
        if(isOwner){
            holder.imgVObjectAction.setVisibility(View.VISIBLE);
            holder.imgVObjectAction.setOnClickListener(view -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onRemove(giftModels.get(position).getId());
                }
            });
        }else{
//            holder.imgVObjectAction.setVisibility(View.GONE);

                if (giftModels.get(position).getBooked() == 0){
                    holder.imgVObjectAction.setImageResource(R.drawable.ic_gift_unbooked);
                    holder.imgVObjectAction.setOnClickListener(v-> {
                        if (onItemClickListener != null) {
                            onItemClickListener.onBooked(giftModels.get(position).getId());
                        }
                    });
                } else {
                    holder.imgVObjectAction.setImageResource(R.drawable.ic_gift_booked);
                    holder.imgVObjectAction.setOnClickListener(v-> {
                        if (onItemClickListener != null) {
                            onItemClickListener.onUndoBooked(giftModels.get(position).getId());
                        }
                    });

                }
        }
        getObject(giftModels.get(position).getProductUrl(),holder,position);
    }

    @Override
    public int getItemCount() {
        return giftModels.size();
    }
    private void getObject(String url, adapterObject.ViewHolder holder, int position) {
        RequestQueue requestQueue = Volley.newRequestQueue(holder.itemView.getContext());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    Gson gson = new Gson();
                    ObjectsModel object = gson.fromJson(response.toString(), ObjectsModel.class);
                    holder.txtVObjectName.setText(object.getName());
                    holder.txtVObjectDescription.setText(object.getDescription());
                    Picasso.get()
                            .load(object.getPhoto())
                            .error(R.drawable.logo)
                            .into(holder.imgVObj);
                    if(isForBookedGift){
                        getWishListTooName(position,holder);
                    }
                },
                error -> {
                    // Manejar el error de la solicitud aquí
                }

        );

        requestQueue.add(request);
    }

    private void getWishListTooName(int position, ViewHolder holder) {
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/wishlists/"+ giftModels.get(position).getWishlistId();
        RequestQueue requestQueue = Volley.newRequestQueue(holder.itemView.getContext());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    Gson gson = new Gson();
                    WishListModel wishList = gson.fromJson(response.toString(), WishListModel.class);
                    holder.txtVObjectDescription.setText(wishList.getName());
                },
                error -> {
                    // Manejar el error de la solicitud aquí
                }

        ){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    public interface OnItemClickListener {
        void  onRemove(int position);
        void onBooked(int position);
        void onUndoBooked(int position);
    }
    public void setOnItemClickListener(adapterObject.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
}