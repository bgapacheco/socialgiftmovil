package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.modelo.ObjectsModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class CreateObject extends AppCompatActivity {
    EditText edtTName, edtTDescription, edtTPrice;
    ImageView imgVBack;
    Button btnAction;
    int objectId;
    int wishListId;
    String token ;
    boolean isCreate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_object);
        btnAction =  findViewById(R.id.btnCreateObject);
        edtTDescription =  findViewById(R.id.edtTDescriptionCreateObject);
        edtTName =  findViewById(R.id.edtTNameCreateObject);
        edtTPrice =  findViewById(R.id.edtTPriceCreateObject);
        imgVBack = findViewById(R.id.imgVObjectActivityBack);
        objectId = getIntent().getIntExtra("objectId",0);
        wishListId = getIntent().getIntExtra("wishListId", 0);

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");

        imgVBack.setOnClickListener(v-> finish());
        if (objectId != 0){
            getObject();
            btnAction.setText(R.string.EditObject);
            isCreate =  false;
        }

        btnAction.setOnClickListener(view -> {
                doAction();
        });

    }

    private void getObject(){

    }
    private void doAction(){
        if (isCreate){
            createObject();
        } else {
            editObject();
        }

    }
    private void createObject(){
        JSONObject jsonWishListCreate = createJsonObjectForCreate();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/mercadoexpress/api/v1/products";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonWishListCreate,
                response -> {
                    try {
                        insertObjectInWishList(Integer.parseInt(response.getString("id")));
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    Toast.makeText(CreateObject.this, R.string.createSuccessObject, Toast.LENGTH_SHORT).show();
                    finish();
                },
                error -> Toast.makeText(CreateObject.this, R.string.createFailiureObject, Toast.LENGTH_SHORT).show()){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    private void insertObjectInWishList(int idObj){
        JSONObject jsonObject =  createJsonObjectForInsert(idObj);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/gifts";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                response -> {
                    Toast.makeText(CreateObject.this, R.string.createSuccessObject, Toast.LENGTH_SHORT).show();
                    finish();
                },
                error -> Toast.makeText(CreateObject.this, R.string.createFailiureObject, Toast.LENGTH_SHORT).show()){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);
    }

    private  JSONObject createJsonObjectForCreate(){
        JSONObject wishList = new JSONObject();
        String strName =  edtTName.getText().toString().trim();
        String strDesc =  edtTDescription.getText().toString().trim();
        double price = Double.parseDouble(edtTPrice.getText().toString());
        try {
            wishList.put("name", strName);
            wishList.put("description", strDesc);
            wishList.put("price", price);
            wishList.put("link", "https://balandrau.salle.url.edu/i3/repositoryimages/thumbnail/1a.png");
            wishList.put("photo", "https://balandrau.salle.url.edu/i3/repositoryimages/thumbnail/1a.png");
        }
        catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return  wishList;

    }
    private  JSONObject createJsonObjectForInsert(int id){
        JSONObject object = new JSONObject();
        try {
            object.put("wishlist_id", wishListId);
            object.put("product_url", "https://balandrau.salle.url.edu/i3/mercadoexpress/api/v1/products/"+id);
            object.put("priority",33);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        return  object;
    }
    private void editObject(){

    }

}