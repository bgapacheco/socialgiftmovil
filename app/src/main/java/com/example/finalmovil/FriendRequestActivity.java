package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.finalmovil.adapters.adapterFriend;
import com.example.finalmovil.adapters.adapterFriendRequest;
import com.example.finalmovil.modelo.UserModel;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class FriendRequestActivity extends AppCompatActivity {
    ImageView imgVBack;
    RecyclerView rVFriendsRequest;
    adapterFriendRequest adapterRecycler;
    String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request);
        imgVBack = findViewById(R.id.imgVAllFriendsRequestBack);
        rVFriendsRequest =  findViewById(R.id.rVAllFriendsRequest);
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        imgVBack.setOnClickListener(v-> finish());

        getFriendRequest();
    }
    private void getFriendRequest(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://balandrau.salle.url.edu/i3/socialgift/api/v1/friends/requests";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    UserModel[] friends = gson.fromJson(response, UserModel[].class);
                    fillRecycler(friends);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization","Bearer "+  token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }

    private void fillRecycler(UserModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rVFriendsRequest.setLayoutManager(layoutManager);
        adapterRecycler = new adapterFriendRequest(myDataSet);
        adapterRecycler.setOnItemClickListener(new adapterFriendRequest.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                showFriend(position);
            }

            @Override
            public void onClickAccept(int position) {
                acceptFriendRequest(position);
            }

            @Override
            public void onClickRemove(int position) {
                declineFriendRequest(position);
            }


        });
        rVFriendsRequest.setAdapter(adapterRecycler);
    }
    private void showFriend(int id){
        Intent intent = new Intent(this, FriendActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    private void acceptFriendRequest(int id){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/friends/"+id;

        StringRequest request = new StringRequest(Request.Method.PUT, url,
                response -> {
                    Toast.makeText(FriendRequestActivity.this, R.string.requestAccepted, Toast.LENGTH_SHORT).show();
                    getFriendRequest();
                    },
                error -> {
                    // Manejar el error de la solicitud
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    private void declineFriendRequest(int id){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/friends/"+id;

        StringRequest request = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    Toast.makeText(FriendRequestActivity.this, R.string.requestRejected, Toast.LENGTH_SHORT).show();
                    getFriendRequest();
                },
                error -> {
                    // Manejar el error de la solicitud
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
}