package com.example.finalmovil;

import static android.content.Context.MODE_PRIVATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterChat;
import com.example.finalmovil.adapters.adapterFriend;
import com.example.finalmovil.modelo.UserModel;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class FragmentChats extends Fragment {
    int id = 0;
    String token = "";
    private adapterChat adapterRecycler;

    RecyclerView rvChats;
    public FragmentChats() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("preferencias", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        Log.d("token", token);
        JWT jwt = new JWT(token);
        id = jwt.getClaim("id").asInt();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_chats, container, false);
        rvChats = rootView.findViewById(R.id.rvChatsFragment);
        return rootView;
    }
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getChats();
        super.onViewCreated(view, savedInstanceState);
    }

    private void getChats(){
        RequestQueue queue = Volley.newRequestQueue(FragmentChats.this.getContext());
        String url ="https://balandrau.salle.url.edu/i3/socialgift/api/v1/messages/users";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    UserModel[] friends = gson.fromJson(response, UserModel[].class);
                    Log.d("hola", String.valueOf(friends.length));
                    llenarRecycler(friends);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization","Bearer "+  token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }
    private void llenarRecycler(UserModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(FragmentChats.this.getContext());
        rvChats.setLayoutManager(layoutManager);
        adapterRecycler = new adapterChat(myDataSet);
        adapterRecycler.setOnItemClickListener(new adapterChat.OnItemClickListener() {
            @Override
            public void onItemClick(int position, String userName) {
                showChatRoom(position,userName);
            }
        });
        Log.d("gifts", String.valueOf(adapterRecycler.getItemCount()));
        rvChats.setAdapter(adapterRecycler);
    }

    private void showChatRoom(int position,String userName){
        Intent intent = new Intent(FragmentChats.this.getContext(), MessageChatActivity.class);
        intent.putExtra("idUser",position);
        intent.putExtra("userName", userName);
        startActivity(intent);
    }
}