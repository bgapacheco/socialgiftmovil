package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.modelo.UserModel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {
    CardView cardFriend, cardWishlist;
    TextView txtVName, txtVEmail;
    ImageView imgVProfileImage, imgVBack,imgVActionFriend,imgVSendMessage;

    private JSONArray friendsResponse;
    String token;
    int idAccountUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        cardFriend =  findViewById(R.id.cardProfileActivityFriends);
        cardWishlist = findViewById(R.id.cardProfileActivityWishList);
        txtVName = findViewById(R.id.txtVProfileActivityName);
        txtVEmail = findViewById(R.id.txtProfileActivityMail);
        imgVProfileImage =  findViewById(R.id.imgVProfileActivityProfileImage);
        imgVBack =  findViewById(R.id.imgVProfileActivityBack);
        imgVActionFriend = findViewById(R.id.imgVProfileActivityActionWithUser);
        imgVSendMessage = findViewById(R.id.imgVProfileActivitySendMessage);

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        JWT jwt = new JWT(token);
        idAccountUser = jwt.getClaim("id").asInt();
        int idUser =  getIntent().getIntExtra("userId", 0);
        if(idUser != idAccountUser) {
            getFriends(idUser);
        }else {
            imgVActionFriend.setVisibility(View.GONE);
            imgVSendMessage.setVisibility(View.GONE);
        }
        imgVSendMessage.setOnClickListener(v-> sendMessage(idUser));
        getUser(idUser);

        cardWishlist.setOnClickListener(v-> {
            Intent intent = new Intent(ProfileActivity.this, AllWishlListActivity.class);
            intent.putExtra("id", idUser);
            startActivity(intent);
        });
        cardFriend.setOnClickListener(v -> {
            Intent intent = new Intent(ProfileActivity.this, FriendActivity.class);
            intent.putExtra("id", idUser);
            startActivity(intent);
        });
        imgVBack.setOnClickListener(v-> finish());

    }

    private void sendMessage(int idUser) {
        Intent intent = new Intent(ProfileActivity.this, MessageChatActivity.class);
        intent.putExtra("idUser", idUser);
        startActivity(intent);
    }

    private void getUser(int id){

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/" + id;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    UserModel userModel = gson.fromJson(response, UserModel.class);
                    txtVName.setText(userModel.getName());
                    txtVEmail.setText(userModel.getEmail());
                    Picasso.get().load(userModel.getImage()).into(imgVProfileImage);
                    Log.d("userSearchedID", "Response is: " + userModel.getName());
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };

        requestQueue.add(stringRequest);
    }
    public boolean searchUserByid(int idBuscado) {
        if (friendsResponse != null) {
            // Recorrer la respuesta de amigos y buscar el ID
            for (int i = 0; i < friendsResponse.length(); i++) {
                try {
                    JSONObject amigo = friendsResponse.getJSONObject(i);
                    int id = amigo.getInt("id");
                    if (id == idBuscado) {
                        return true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public void getFriends(int idBuscado) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/friends";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        friendsResponse = response;
                        // Verificar si el ID coincide con algún usuario
                        boolean encontrado = searchUserByid(idBuscado);
                        if (encontrado) {
                            imgVActionFriend.setImageResource(R.drawable.ic_delete_user);
                            imgVActionFriend.setOnClickListener(v->{
                                deleteFriend();
                            });
                        } else {
                            imgVActionFriend.setImageResource(R.drawable.ic_user_add);
                            imgVActionFriend.setOnClickListener(v->{
                                addFriend();
                            });
                        }
                    }
                },
                error -> {

                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token); // Agregar el encabezado con el token
                return headers;
            }
        };

        requestQueue.add(request);
    }

    private void deleteFriend(){
        int idUser =  getIntent().getIntExtra("userId", 0);
        deleteFriendRequest(idUser);
        Toast.makeText(ProfileActivity.this,R.string.friendDeleted, Toast.LENGTH_SHORT).show();
    }

    private void deleteFriendRequest(int id) {
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/friends/"+id;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    getFriends(id);
                    Toast.makeText(ProfileActivity.this,R.string.requestRejected, Toast.LENGTH_SHORT).show();
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };

        requestQueue.add(stringRequest);


    }

    private void addFriend(){
        int idUser =  getIntent().getIntExtra("userId", 0);
        addFriendRequest(idUser);
    }

    private void addFriendRequest(int id) {
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/friends/"+id;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    Toast.makeText(ProfileActivity.this,R.string.registerSend, Toast.LENGTH_SHORT).show();
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };

        requestQueue.add(stringRequest);
    }
}