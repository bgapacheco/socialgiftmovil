
package com.example.finalmovil.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("user_id_send")
    @Expose
    private Integer userIdSend;
    @SerializedName("user_id_recived")
    @Expose
    private Integer userIdRecived;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getUserIdSend() {
        return userIdSend;
    }

    public void setUserIdSend(Integer userIdSend) {
        this.userIdSend = userIdSend;
    }

    public Integer getUserIdRecived() {
        return userIdRecived;
    }

    public void setUserIdRecived(Integer userIdRecived) {
        this.userIdRecived = userIdRecived;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

}
