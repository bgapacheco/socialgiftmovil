package com.example.finalmovil.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GiftModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("wishlist_id")
    @Expose
    private Integer wishlistId;
    @SerializedName("product_url")
    @Expose
    private String productUrl;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("booked")
    @Expose
    private Integer booked;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(Integer wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getBooked() {
        return booked;
    }

    public void setBooked(Integer booked) {
        this.booked = booked;
    }

}
