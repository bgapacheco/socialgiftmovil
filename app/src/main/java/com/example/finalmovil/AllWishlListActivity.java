package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterWishlist;
import com.example.finalmovil.modelo.WishListModel;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
//"https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/"+6+"/wishlists"
public class AllWishlListActivity extends AppCompatActivity {
    TextView txtVTitle;
    ImageView imgVBack;
    ImageView imgVAdd;
    RecyclerView rvWishlist;
    String token;
    int id;
    private adapterWishlist adapterRecycler;

    boolean isOwner = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_wishl_list);
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        Log.d("toke", token);
        JWT jwt = new JWT(token);
        id = jwt.getClaim("id").asInt();
        imgVBack =  findViewById(R.id.imgVAllWishListBack);
        imgVAdd = findViewById(R.id.imgVAllWishListAddNewObj);
        rvWishlist =  findViewById(R.id.rVAllWishList);
        if(id != getIntent().getIntExtra("id", 0)){
            imgVAdd.setVisibility(View.GONE);
            isOwner= false;
            id = getIntent().getIntExtra("id", 0);
        }else {
            imgVAdd.setOnClickListener(view -> showAddWishList());

        }
        txtVTitle= findViewById(R.id.txtVAllWishWishListName);
        imgVBack.setOnClickListener(view -> finish());
        getWishList();
    }
    public void onResume(){
        super.onResume();
        getWishList();
    }
    private void showAddWishList(){
        Intent intent = new Intent(AllWishlListActivity.this, CreateWishListActivity.class);
        startActivity(intent);
    }
    public void getWishList(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/"+id+"/wishlists";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    WishListModel[] wishlist = gson.fromJson(response, WishListModel[].class);
                    llenarRecycler(wishlist);
                    Log.d("wishlistsNum", "Response is: " + wishlist.length);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }
    private void llenarRecycler(WishListModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvWishlist.setLayoutManager(layoutManager);
        adapterRecycler = new adapterWishlist(myDataSet,isOwner);
        Log.d("gifts", String.valueOf(adapterRecycler.getItemCount()));
        adapterRecycler.setOnItemClickListener(new adapterWishlist.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                // Manejar el evento de click del adaptador aquí
                showWishlist(position);
                Log.d("IdWishItem", String.valueOf(position));
            }

            @Override
            public void onRemove(int position) {
                Log.d("IdWishRemove", String.valueOf(position));
                deleteWishList(position);

            }

            @Override
            public void onEdit(int position) {
                sendToEditWishList(position);
            }
        });

        rvWishlist.setAdapter(adapterRecycler);
    }
    private void showWishlist(int id){
        Intent intent = new Intent(this, WishListActivity.class);
        intent.putExtra("wishlistId", id);
        intent.putExtra("isOwner", isOwner);
        startActivity(intent);
    }
    private void sendToEditWishList(int id){
        Intent intent = new Intent(this, CreateWishListActivity.class);
        intent.putExtra("wishlistId", id);
        startActivity(intent);
    }

    private void deleteWishList(int id){
        RequestQueue requestQueue = Volley.newRequestQueue(AllWishlListActivity.this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/wishlists/"+id;
        StringRequest request = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    getWishList();
                    Toast.makeText(AllWishlListActivity.this, R.string.deletedWishlist, Toast.LENGTH_SHORT).show();
                },
                error -> {
                    // Manejar el error de la solicitud
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);

    }

}