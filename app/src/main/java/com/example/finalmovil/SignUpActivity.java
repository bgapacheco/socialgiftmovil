package com.example.finalmovil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.*;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.modelo.UserModel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {
    private TextView txtVName, txtVLastname, txtVEmail, txtVPassword, txtVBackToLogin;
    private EditText edtTName, edtTLastname, edtTEmail, edtTPassword;
    private Button btnRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        linkViews();
        txtVBackToLogin.setOnClickListener(view -> sendToLogin());
        btnRegister.setOnClickListener(view -> {
            try {
                postUser();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });

    }

    private void linkViews() {
        txtVName = findViewById(R.id.txtVNameSignUp);
        txtVLastname = findViewById(R.id.txtVLastnameSignUp);
        txtVEmail = findViewById(R.id.txtVEmailSignUp);
        txtVPassword = findViewById(R.id.txtVPasswordSignUp);
        txtVBackToLogin = findViewById(R.id.txtVBackToLogin);

        edtTName = findViewById(R.id.edtTNameSignUp);
        edtTLastname = findViewById(R.id.edtTLastnameSignUp);
        edtTEmail = findViewById(R.id.edtTEmailSignUp);
        edtTPassword = findViewById(R.id.edtTPasswordSignUp);

        btnRegister = findViewById(R.id.btnRegisterSignUp);
    }

    private void postUser() throws JSONException {
        String name = edtTName.getText().toString();
        String lastname = edtTLastname.getText().toString();
        String email = edtTEmail.getText().toString();
        String password = edtTPassword.getText().toString();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("last_name", lastname);
        jsonObject.put("email", email);
        jsonObject.put("password", password);
        jsonObject.put("image", "https://balandrau.salle.url.edu/i3/repositoryimages/photo/47601a8b-dc7f-41a2-a53b-19d2e8f54cd0.png");
        postRequest(jsonObject);
    }

    private void postRequest(JSONObject jsonUser){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonUser,
                response -> {
                    Toast.makeText(SignUpActivity.this, R.string.UserCorrect, Toast.LENGTH_SHORT).show();
                    sendToLogin();
                },
                error -> Toast.makeText(SignUpActivity.this, R.string.GenericError, Toast.LENGTH_SHORT).show());

        requestQueue.add(request);
    }

    private void sendToLogin(){
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("email");
        editor.apply();
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        intent.putExtra("email", edtTEmail.getText().toString());
        startActivity(intent);
        finish();
    }

}