package com.example.finalmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.auth0.android.jwt.JWT;
import com.example.finalmovil.adapters.adapterObject;
import com.example.finalmovil.adapters.adapterObjectAddWishList;
import com.example.finalmovil.modelo.GiftModel;
import com.example.finalmovil.modelo.ObjectsModel;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookedGiftsActivity extends AppCompatActivity {

    int id;
    ImageView imgVBack;
    RecyclerView rvGift;
    String token;
    adapterObject adapterRecycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booked_gifts);
        imgVBack = findViewById(R.id.imgVBookedGiftBack);
        rvGift =  findViewById(R.id.rVBookedGift);
        SharedPreferences sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        JWT jwt = new JWT(token);
        id = jwt.getClaim("id").asInt();
        getBookedGifts();

        imgVBack.setOnClickListener(v-> finish());

    }
    private void getBookedGifts(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/users/"+id+"/gifts/reserved";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    Gson gson = new Gson();
                    GiftModel[] objects = gson.fromJson(response, GiftModel[].class);
                    llenarRecycler(objects);
                },
                error -> {
                    Log.e("Error", ""+ error.networkResponse.statusCode);
                }
        ){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };
        queue.add(stringRequest);
    }

    private void llenarRecycler(GiftModel[] myDataSet) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvGift.setLayoutManager(layoutManager);
        adapterRecycler = new adapterObject( Arrays.asList(myDataSet),false,true,token);
        adapterRecycler.setOnItemClickListener(new adapterObject.OnItemClickListener() {
            @Override
            public void onRemove(int position) {

            }

            @Override
            public void onBooked(int position) {

            }

            @Override
            public void onUndoBooked(int position) {
                undoReserveObject(position);
            }

        });
        rvGift.setAdapter(adapterRecycler);

    }
    private void undoReserveObject(int position) {
        String url = "https://balandrau.salle.url.edu/i3/socialgift/api/v1/gifts/"+position+"/book";
        RequestQueue requestQueue = Volley.newRequestQueue(BookedGiftsActivity.this);

        StringRequest request = new StringRequest(Request.Method.DELETE, url,
                response -> {
                    getBookedGifts();
                    Toast.makeText(BookedGiftsActivity.this, R.string.bookedDeleted, Toast.LENGTH_SHORT).show();
                },
                error -> {

                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " +token);
                return headers;
            }
        };
        requestQueue.add(request);

    }
}